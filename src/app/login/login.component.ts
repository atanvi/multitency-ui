import { AppService } from './../app.service';
import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { Router } from '@angular/router';
import { FormControl, FormGroup } from '@angular/forms';
import { StorageService } from './../storage.service';
import {StorageType} from './../storage-type.enum';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private router: Router,private appService:AppService,private storageService:StorageService) {

  }
  loginForm = new FormGroup ({
    email: new FormControl(),
    password:new FormControl(),
  });

  ngOnInit() {
  

  }
  

  login(){
      console.log(this.loginForm.get('email').value);
      console.log(this.loginForm.get('password').value);

      this.appService.login(this.loginForm.value).subscribe(resp => {
        if (resp['token'] !== null) {
          this.storageService.put('token', resp, StorageType.LOCALSTORAGE);
          this.router.navigateByUrl("/home");
        } else {
           console.log("Invalid credentials");
        }
      }, err => {
        console.log(err);
      })
    }

}
