import { AppService } from './../../app.service';
import { Component, OnInit } from '@angular/core';


@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})
export class ListComponent implements OnInit {
  faculityList:any =[];
  
  constructor(private appService:AppService) { 
    this.showFacultyList();

  }
  ngOnInit() {

  }

  showFacultyList(){
    this.appService.getFaculty().subscribe(resp => {
      this.faculityList = resp;
      console.log("Faculty list",this.faculityList);
    }, err => {
      console.log('Not able to get faculty list');
    });
  }

}


