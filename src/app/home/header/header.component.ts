
import { AppService } from './../../app.service';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { StorageService } from './../../storage.service';
import {StorageType} from './../../storage-type.enum';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  user={};
  constructor(private router: Router,private appService:AppService,private storageService:StorageService) {
    this.getUserDetails();
  }

  ngOnInit() {
  }

 


getUserDetails(){
 

  this.appService.getLoginUser().subscribe(resp => {
       this.user=resp;
      }, err => {
    console.log(err);
  })
}

  logout() {
    this.storageService.removeAll(StorageType.LOCALSTORAGE);
    this.storageService.removeAll(StorageType.SESSIONSTORAGE);
    this.storageService.removeAll(StorageType.COOKIESTORAGE);
    this.router.navigate(['']);
  }

}
