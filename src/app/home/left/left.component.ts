import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-left',
  templateUrl: './left.component.html',
  styleUrls: ['./left.component.css']
})
export class LeftComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit() {
  }
  showlist(){
      this.router.navigateByUrl("home/list");
  }

}
