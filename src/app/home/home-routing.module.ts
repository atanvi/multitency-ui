import { RootAuthGuardGuard } from './../guard/root-auth-guard.guard';
import { LoginGuard } from './../guard/login.guard';
import { HomeComponent } from './home.component';
import { ListComponent } from './list/list.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path:'',
  component:HomeComponent,
 
  children:[
    {path:'list',
    component:ListComponent
  
  }

  ]
}

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
