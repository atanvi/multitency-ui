import { LeftComponent } from './left/left.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule} from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { ListComponent } from './list/list.component';

@NgModule({
  imports: [
    CommonModule,
    HomeRoutingModule

  ],
  declarations: [ ListComponent,HomeComponent,HeaderComponent,LeftComponent]
})
export class HomeModule { }
