import { StorageService } from './storage.service';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { StorageType } from './storage-type.enum';
import { Headers, Http, RequestOptionsArgs, Response, RequestOptions, ResponseContentType } from '@angular/http';
@Injectable()
export class AppService {

  private apiEndPoint="http://localhost:8080/"
  constructor(private http: Http, private storageService: StorageService) { }


  login(credentials: any): Observable<Response> {
    console.log("Credentials ",credentials)
    return this.post(this.apiEndPoint + 'auth/login', credentials)
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error));
  }

  getFaculty(): Observable<Response> {
    return this.get(this.apiEndPoint + 'api/viewlist')
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error));
  }
  getLoginUser(): Observable<Response> {
    return this.get(this.apiEndPoint + 'api/userdetails')
      .map((res: Response) => res.json())
      .catch((error: any) => Observable.throw(error));
  }

  post(url: string, body: Object, options?: RequestOptionsArgs): Observable<Response> {
   
    return this.http.post(url, body, this.getToken(options))
      .map((res: Response) => {
       
        return res;
      })
     .catch((error: any) =>{
    
        return Observable.throw(error.json())
      });
  }

  get(url: string, options?: RequestOptionsArgs): Observable<Response> {
   
    return this.http.get(url, this.getToken(options))
      .map((res: Response) => {
          return res;
      })
      .catch((error: any) =>{
        return Observable.throw(error.json())
      });
  };

  getToken(options) {
    let token = this.storageService.getByKey('token', StorageType.LOCALSTORAGE);
    if (token !== null && options) {
      let opt = new RequestOptions({ headers: new Headers({ 'Authorization': 'Bearer ' + token['token'] }) });
      options = opt.merge(options);
    } else if (token !== null && !options) {
      options = new RequestOptions({ headers: new Headers({ 'Authorization': 'Bearer ' + token['token'] }) });
    }
    return options;
  }

}
