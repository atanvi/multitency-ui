import { LoginGuard } from './guard/login.guard';
import { RootAuthGuardGuard } from './guard/root-auth-guard.guard';
import { StorageService } from './storage.service';
import { Http,HttpModule } from '@angular/http';
import { AppService } from './app.service';
import { AppComponent } from './app.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';

import { BrowserModule } from '@angular/platform-browser';
@NgModule({
  imports: [
    CommonModule,
    AppRoutingModule,
    BrowserModule,
    ReactiveFormsModule,
    HttpModule
  ],
  declarations: [AppComponent,  LoginComponent ],
  providers:[AppService,StorageService,RootAuthGuardGuard, LoginGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
