import { StorageType } from './../storage-type.enum';
import { StorageService } from './../storage.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { Router } from '@angular/router';
@Injectable()
export class RootAuthGuardGuard implements CanActivate {

  constructor(private storageService: StorageService, private router: Router) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    let token = this.storageService.getByKey('token', StorageType.LOCALSTORAGE);
    if (token !== null && token['token']) {
      return true;
    }
    this.router.navigate(['/login']);
    return false;
  }
}
